import logging
import sys

from PySide6.QtCore import QLoggingCategory, Slot
from PySide6.QtGui import QCloseEvent
from PySide6.QtWebEngineCore import QWebEnginePage
from PySide6.QtWebEngineWidgets import QWebEngineView
from PySide6.QtWidgets import QApplication, QMainWindow

logger = logging.getLogger(__name__)

ACCESS_PATH = "/index.html"


class BrowserWindow(QMainWindow):
    def __init__(self, host: str, *args, **kwargs):
        path = host + ACCESS_PATH
        logger.info("Opening %s", path)

        super().__init__(*args, **kwargs)

        webview = QWebEngineView()
        QLoggingCategory.setFilterRules("qt.webenginecontext=false")

        webpage = QWebEnginePage(webview)
        self.webpage = webpage

        webview.setPage(webpage)
        webview.setUrl(path)
        self.webview = webview

        self.setCentralWidget(webview)
        self.setWindowTitle("Game")

    @Slot(QCloseEvent)
    def closeEvent(self, _):
        self.webpage.deleteLater()

def main(dir):
    app = QApplication.instance() or QApplication()
    w = BrowserWindow("file://" + dir)
    w.show()
    logger.debug("BrowserWindow.exec")
    app.exec()

if __name__ == "__main__":
    main(sys.argv[-1])
