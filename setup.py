from setuptools import setup


setup(
    name="pyfdc",
    version="0.0",
    packages=["pyfdc"],
    url="https://gitlab.com/VisaJE/pyfdc",
    license="Dunno",
    author="Eemil Visakorpi",
    author_email="eemil@port6.io",
    description="",
    install_requires=["pyside6==6.4.3"],
)
