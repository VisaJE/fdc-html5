#! /bin/bash

# Prepare python environment for easy running

dir=$(CDPATH= cd -- "$(dirname -- "$0")" && pwd)

if [ $PYTHON_EXE ]
then
    echo "Using supplied python executable"
elif command -v python &> /dev/null
then
    PYTHON_EXE=python
elif command -v python3 &> /dev/null
then
    PYTHON_EXE=python3
else
    echo "Python not found in path. Is it installed?"
    exit 1
fi

if ! $PYTHON_EXE -m venv -h >/dev/null
then
    echo "Python virtual env not installed."
    exit 1
fi

if ! $PYTHON_EXE -m pip -h >/dev/null
then
    echo "Pip missing, is python correctly installed?"
    exit 1
fi

if [ -d $dir/env ]
then
    echo "Using existing virtual env"
else
    $PYTHON_EXE -m venv $dir/env
fi
. $dir/env/bin/activate || exit 1

$PYTHON_EXE -m pip install -e . || exit 1

EXE_NAME="fdc-html5.sh"
if [ -f $EXE_NAME ]
then
  rm $EXE_NAME
fi
echo "#! $dir/env/bin/python" >> $EXE_NAME
echo "import pyfdc.web_engine" >> $EXE_NAME
echo "pyfdc.web_engine.main('$dir')" >> $EXE_NAME
chmod 700 $EXE_NAME
sudo mv $EXE_NAME /usr/bin/
